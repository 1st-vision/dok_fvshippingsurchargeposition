.. |label| replace:: Versandaufschlag als Bestellposition
.. |snippet| replace:: FvShippingSurchargePosition
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.4
.. |version| replace:: 1.0.3
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht es, einen im Shopware Backend definierten Versandaufschlag als Bestellposition anzuzeigen und diese ebenso zur Office Line zu exportieren.

Frontend
--------
Im Warenkorb wird dieser Versandaufschlag als Position angezeigt und nicht in die Versandkosten aufaddiert.



Backend
-------

.. image:: FvShippingSurchargePosition1.png

Grundeinstellungen
__________________
.. image:: FvShippingSurchargePosition2.png

:Titel der Zuschlags-Position: Hier hinterlegen Sie den Titel, der in der Position angezeigt werden soll
:Bestellnummer der Zuschlags-Position: Hier hinterlegen Sie die Bestellnummer, die in der Position angezeigt werden soll



Textbausteine
_____________

keine


technische Beschreibung
------------------------
keine


Modifizierte Template-Dateien
-----------------------------
keine





